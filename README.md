# SiliconValleyEpisodes

## Name
SiliconValleyEpisodeGuide.

## Description
This app serves as a comprehensive guide to all episodes of the TV series "Silicon Valley." The app provides an easy-to-use interface for users to browse through episodes, offering detailed information about each one.

## Features
Episode Overview: The app presents a list view of all episodes, displaying the cover image, season number, episode number, and episode name for each entry.
Episode Details: Upon selecting an episode from the overview, users are taken to a detailed view that includes the cover image, season and episode numbers, episode name, and a summary of the episode.
Data-Driven Design: The app dynamically fetches and displays data based on a JSON file, ensuring that the content is both up-to-date and comprehensive.

## Technical Details
Platform: iOS
Languages: SwiftUi
Architecture: MVVM (Model-View-ViewModel)
UI Design: SwiftUI
Data Handling: Custom service to parse and handle JSON data
