//
//  Series.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

/// `Series` represents the show.
struct Series: Codable {
	let seasons: [Season]
	let title: String
}

/// `Season` represents a single season within the show.
struct Season: Codable {
	var episodes: [Episode]
}

