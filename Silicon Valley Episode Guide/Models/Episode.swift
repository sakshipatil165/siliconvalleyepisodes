//
//  Episode.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

/// `Episode` represents a single episode within the show providing information related to it.
struct Episode: Codable, Identifiable {
	var id: String { imdbID }
	let poster: String
	let season: String
	let episode: String
	let title: String
	let plot: String
	let runtime: String
	let released: String
	let genre: Genre
	let imdbID: String

	enum CodingKeys: String, CodingKey {
		case plot = "Plot"
		case title = "Title"
		case episode = "Episode"
		case season = "Season"
		case poster = "Poster"
		case imdbID
		case runtime = "Runtime"
		case released = "Released"
		case genre = "Genre"
	}
}

enum Genre: String, Codable {
	case comedy = "Comedy"
	case nA = "N/A"
}
