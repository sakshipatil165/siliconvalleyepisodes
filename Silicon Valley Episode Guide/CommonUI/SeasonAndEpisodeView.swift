//
//  SeasonAndEpisodeView.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import SwiftUI

// Displays the title and season/episode information.
struct SeasonAndEpisodeView: View {
	let season: String
	let episode: String
	
	var body: some View {
		HStack {
			Text("Episode: \(episode)")
			Text("Season: \(season)")
		}
		.font(.caption)
		.bold()
		.padding(.bottom)
	}
}

#Preview {
	SeasonAndEpisodeView(season: "1", episode: "4")
}
