//
//  SiliconValleyEpisodeGuideApp.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import SwiftUI

@main
struct SiliconValleyEpisodeGuideApp: App {
    var body: some Scene {
        WindowGroup {
            EpisodesListView()
        }
    }
}
