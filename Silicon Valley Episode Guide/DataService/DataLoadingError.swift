//
//  DataLoadingError.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

// Enum defining various errors that can occur while loading and processing data.
enum DataLoadingError: Error {
	case fileNotFound
	case decodingError
	case unknownError
}

extension DataLoadingError: LocalizedError {
	var errorDescription: String? {
		switch self {
		case .fileNotFound:
			return "The requested file was not found."
			
		case .decodingError:
			return "There was an error decoding the data."
			
		case .unknownError:
			return "An unknown error occurred."
		}
	}
}
