//
//  DataService.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

/// Utility struct providing data loading function.
struct DataService {
	
	/// Fetches the series data from a local JSON file and decodes it into a `Series` object.
	func fetchSeries() async throws -> Series {
		// Check for the existence of the JSON file in the app bundle and throw an error if it's not found.
		guard let url = Bundle.main.url(forResource: "hbo-silicon-valley", withExtension: "json") else {
			throw DataLoadingError.fileNotFound
		}
		
		let data = try Data(contentsOf: url)
		let decoder = JSONDecoder()
		let series = try decoder.decode(Series.self, from: data)
		return series
	}
}

