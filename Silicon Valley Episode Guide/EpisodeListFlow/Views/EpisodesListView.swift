//
//  EpisodesListView.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 02/12/2023.
//

import SwiftUI

struct EpisodesListView: View {
	
	@StateObject var viewModel = EpisodesListViewModel()
	
	// MARK: - Body
	var body: some View {
		NavigationView {
			List {
				Section("Episodes") {
					ForEach(viewModel.episodes) { episode in
						NavigationLink {
							EpisodeDetailsView(episode: episode)
						} label: {
							EpisodeRowView(episode: episode)
						}
						.buttonStyle(PlainButtonStyle())
					}
				}
			}
			.alert(isPresented: .constant(viewModel.errorMessage != nil)) {
				alertData
			}
			.listStyle(.plain)
			.navigationTitle(viewModel.seriesTitle)
			.navigationBarTitleDisplayMode(.inline)
			.onAppear{
				viewModel.onAppear()
			}
		}
	}
	
	// Alert with description for when there is some error fetching the data.
	private var alertData: Alert {
		Alert(
			title: Text("Error"),
			message: Text(viewModel.errorMessage ?? "An unknown error occurred"),
			dismissButton: .default(Text("OK")))
	}
}

#Preview {
	EpisodesListView()
}
