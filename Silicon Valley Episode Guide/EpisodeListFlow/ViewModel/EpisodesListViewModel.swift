//
//  EpisodesListViewModel.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

/// `EpisodesListViewModel` is responsible for fetching and holding the data for the `Series`.
class EpisodesListViewModel: ObservableObject {
	
	/// Holds the data for the series including all seasons and episodes.
	@Published var series: Result<Series, DataLoadingError>?
	
	/// Holds a string describing the error, if any occurs during data fetching.
	@Published var errorMessage: String?
	
	/// Handles all data fetching operations related to the series.
	private var dataService: DataService
	
	/// Consolidates and returns all episodes across all seasons.
	var episodes: [Episode] {
		switch series {
		case .success(let series):
			return series.seasons.flatMap { $0.episodes }
		case .failure, nil:
			return []
		}
	}
	
	/// Provides the title of the series if it exists.
	var seriesTitle: String {
		(try? series?.get())?.title ?? ""
	}
	
	// MARK: - Init.
	
	init(dataService: DataService = DataService()) {
		self.dataService = dataService
	}
	
	// MARK: - Lifecycle events.
	
	/// Asynchronously fetches the series data when the view appears.
	/// It handles both successful data retrieval and error scenarios.
	@MainActor
	func onAppear() {
		Task {
			do {
				let fetchedSeries = try await dataService.fetchSeries()
				self.series = .success(fetchedSeries)
			} catch {
				if let dataLoadingError = error as? DataLoadingError {
					self.series = .failure(dataLoadingError)
				} else {
					self.errorMessage = error.localizedDescription
				}
			}
		}
	}
}
