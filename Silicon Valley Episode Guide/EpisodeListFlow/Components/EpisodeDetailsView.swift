//
//  EpisodeDetailsView.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import SwiftUI

struct EpisodeDetailsView: View {
	
	/// Holds the information about the episode that this view will be displaying.
	let episode: Episode
	
	// MARK: - Body
	
    var body: some View {
		ScrollView {
			VStack(alignment: .leading) {
				title
				SeasonAndEpisodeView(season: episode.season, episode: episode.episode)
				coverImage
				plot
				episodeMetadata
				Spacer()
			}
			.padding(.horizontal)
		}
    }
	
	// MARK: - Private Views
	
	// Displays title of the episode.
	private var title: some View {
		Text(episode.title)
			.font(.title)
			.bold()
			.lineLimit(1)
			.fontWeight(.medium)
	}
	
	// Displays the runtime of the episode.
	private var episodeMetadata: some View {
		HStack(spacing: 0) {
			Text(episode.runtime + " • ")
				.font(.caption)
				.frame(height: 3)
			Text(episode.released + " • ")
				.font(.caption)
			Text(episode.genre.rawValue)
				.font(.caption)
		}
	}
	
	// Displays the plot of the episode.
	private var plot: some View {
		Text(episode.plot)
			.font(.body)
			.fontWeight(.medium)
			.padding(.top)
	}
	
	private var coverImage: some View {
		AsyncImage(url: URL(string: episode.poster)) { image in
			image.resizable()
				.cornerRadius(10.0)
				.aspectRatio(contentMode: .fit)
				.shadow(radius: 3.0)
		} placeholder: {
			ProgressView()
		}
	}
}

#Preview {
	EpisodeDetailsView( episode: .mock)
}
