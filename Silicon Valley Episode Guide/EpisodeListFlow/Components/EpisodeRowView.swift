//
//  EpisodeRowView.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import SwiftUI

struct EpisodeRowView: View {
	
	/// Holds the information about the episode that this view will be displaying.
	let episode: Episode
	
	// MARK: - Body
	
    var body: some View {
		HStack(alignment: .top) {
			episodePoster(imageTitle: episode.poster)
			episodeTitle
			Spacer()
		}
    }
	
	// MARK: - Private Views
	
	// Displays the title and season/episode information.
	private var episodeTitle: some View {
		VStack(alignment: .leading) {
			Text(episode.title)
				.font(.headline)
			SeasonAndEpisodeView(season: episode.season, episode: episode.episode)
		}
	}
	
	// Displays the poster of the episode.
	private func episodePoster(imageTitle: String) -> some View {
		AsyncImage(url: URL(string: imageTitle)) { image in
			image.resizable()
		} placeholder: {
			ProgressView()
		}
		.cornerRadius(5.0)
		.frame(width: 130,height: 80)
		.shadow(radius: 10)
		.aspectRatio(contentMode: .fill)
	}
}

#Preview {
	EpisodeRowView( episode: .mock)
}

