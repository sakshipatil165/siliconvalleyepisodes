//
//  MockData.swift
//  Silicon Valley Episode Guide
//
//  Created by Sakshi Patil on 04/12/2023.
//

import Foundation

extension Episode {
	static let mock = Episode(
		poster: "https://images-na.ssl-images-amazon.com/images/M/MV5BNTNjZGQ1ZTctMDQwYS00MTVmLWE3OWUtNzM0YTFlNDcyNDYwXkEyXkFqcGdeQXVyNjc5Mjg0NjU@._V1_SX300.jpg",
		season: "4",
		episode: "8",
		title: "The Keenan Vortex",
		plot: "An unexpected increase in data traffic leads Richard to turn to Ehrlich for help in garnering a deal with Keenan Feldspar, Silicon Valley's latest 'it' boy, but when he makes a ...",
		runtime: "28 min",
		released: "23 Apr 2017",
		genre: .comedy,
		imdbID: "tt6267510"
	)
}
